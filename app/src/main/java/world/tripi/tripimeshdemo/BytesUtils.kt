package world.tripi.tripimeshdemo

import java.nio.charset.Charset

object BytesUtils {

    fun stringToBytes(string: String): ByteArray{
        return string.toByteArray(Charset.defaultCharset())
    }

    fun bytesToString(bytes: ByteArray): String{

        return bytes.toString(Charset.defaultCharset())
    }
}