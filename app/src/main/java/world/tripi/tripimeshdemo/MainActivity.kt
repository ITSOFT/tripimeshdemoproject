package world.tripi.tripimeshdemo

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import kotlinx.android.synthetic.main.activity_main.*
import world.tripi.ble_mesh.TripiMesh
import world.tripi.ble_mesh.audio.TripiAudioManager
import world.tripi.ble_mesh.audio.TripiAudioManager.AudioState
import world.tripi.ble_mesh.audio.TripiAudioManager.StartVoiceBroadcastingErrorCode
import world.tripi.ble_mesh.audio.TripiPlayer
import world.tripi.ble_mesh.audio.TripiRecorder
import java.util.*

class MainActivity : AppCompatActivity(), TripiMesh.Callback {

    private val TAG = MainActivity::class.java.simpleName
    private val REQUEST_RECORD_AUDIO_PERMISSION = 200
    private val REQUEST_ACCESS_LOCATION_PERMISSION = 400

    private val isMaster = true

    private var mesh: TripiMesh? = null
    private var audioManager: TripiAudioManager? = null
    private var player: TripiPlayer? = null
    private var recorder: TripiRecorder? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        startMesh()
        iniViews()
    }

    private fun startMesh() {
        //you can generate any UUID and use it as your application id
        mesh = TripiMesh.getInstance(
            this,
            UUID.fromString(resources.getString(R.string.app_uuid)),
            this
        )
        audioManager = mesh?.getAudioManager()
        player = audioManager?.getPlayer()
        recorder = audioManager?.getRecorder()
        mesh?.start(1738, isMaster)
    }

    private fun iniViews() {
        bSendTextMessage.setOnClickListener {
            sendTextMessage(
                etTextMessage.text?.toString() ?: ""
            )
        }
        bStartVoiceBroadcasting.setOnClickListener {
            audioManager?.startVoiceBroadcasting(
                startVoiceCallback
            )
        }
        bStopVoiceBroadcasting.setOnClickListener { audioManager?.stopVoiceBroadcasting() }
        sbBoost.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (fromUser)
                    recorder?.setBoost(progress.toDouble())
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {}

            override fun onStopTrackingTouch(seekBar: SeekBar?) {}

        })
        sMute.setOnCheckedChangeListener { _, isChecked ->
            player?.setMute(isChecked)
        }

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_ACCESS_LOCATION_PERMISSION -> if (grantResults.isNotEmpty()) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)
                    mesh?.start(1738, isMaster)
            }
            REQUEST_RECORD_AUDIO_PERMISSION -> if (grantResults.isNotEmpty()) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    audioManager?.startVoiceBroadcasting(startVoiceCallback)
            }
        }
    }

    private fun sendTextMessage(message: String) {
        mesh?.sendDataToNeighbors(BytesUtils.stringToBytes(message))
    }

    private val startVoiceCallback = object : TripiAudioManager.StartVoiceCallback {
        override fun onError(code: Int) {
            Log.i(TAG, "TripiMesh: startVoiceBroadcasting() -> onError($code)")
            when (code) {
                StartVoiceBroadcastingErrorCode.NEED_PERMISSIONS_FOR_RECORDING -> requestPermissionsRecording()

                StartVoiceBroadcastingErrorCode.NOT_MASTER_START_RECORDING -> {
                }

                StartVoiceBroadcastingErrorCode.BROADCASTING_ALREADY_STARTED -> {
                }

                StartVoiceBroadcastingErrorCode.OTHER_MASTER_IS_BROADCASTING -> {
                }

                StartVoiceBroadcastingErrorCode.NOT_SUPPORT_RECORDING -> {
                }

                StartVoiceBroadcastingErrorCode.CAN_NOT_START_RECORDING -> {
                }
            }
        }

        override fun onSuccess() {
            Log.i(TAG, "TripiMesh.Callback: startVoiceBroadcasting() -> onSuccess()")
        }

    }

    override fun onStarted() {
        Log.i(TAG, "TripiMesh: onStarted()")
        runOnUiThread {
            tvNodeId.text = "NodeId: " + mesh?.getNodeId()
            tvIsMaster.text = "Is master: $isMaster"
        }
    }

    override fun onStartFailed(errorCode: Int) {
        Log.i(TAG, "TripiMesh: onStartFailed($errorCode)")
        when (errorCode) {
            TripiMesh.StartMeshErrorCode.NEED_BLUETOOTH_PERMISSIONS -> requestPermissionsLocation()
        }
    }

    override fun onAudioStateChanged(state: AudioState) {
        Log.i(TAG, "TripiMesh: onAudioStateChanged($state)")
        runOnUiThread {
            tvAudioState.text = "Audio state: $state"
            when (state) {
                AudioState.BROADCASTING -> {
                    llBost.visibility = View.VISIBLE
                    llMute.visibility = View.GONE
                    sbBoost.progress = recorder?.getBoost()?.toInt() ?: 0
                }
                AudioState.PLAYING -> {
                    llBost.visibility = View.GONE
                    llMute.visibility = View.VISIBLE
                    sMute.splitTrack = player?.isMute() ?: false
                }
                AudioState.INACTIVE -> {
                    llBost.visibility = View.GONE
                    llMute.visibility = View.GONE
                }
            }
        }
    }


    override fun onConnectionStatusChanged(status: TripiMesh.ConnectionStatus) {
        Log.i(TAG, "TripiMesh: onConnectionStatusChanged($status)")
        runOnUiThread { tvConnectionStatus.text = "Connection status: $status" }
    }

    override fun onDataReceived(data: ByteArray) {
        Log.i(TAG, "TripiMesh: onDataReceived(${data.contentToString()}))")
        runOnUiThread {
            tvReceivedTextMessage.text = "Received message: " + BytesUtils.bytesToString(data)
        }
    }

    override fun onConnectedDevicesListChanged(devices: List<Int>) {
        Log.i(TAG, "TripiMesh: onConnectedDevicesListChanged($devices)")
        runOnUiThread {
            tvConnectedDevices.text = "Connected devices: $devices"
        }
    }

    private fun requestPermissionsLocation() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            ActivityCompat.requestPermissions(
                this, arrayOf(
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_BACKGROUND_LOCATION
                ),
                REQUEST_ACCESS_LOCATION_PERMISSION
            )
        } else {
            ActivityCompat.requestPermissions(
                this, arrayOf(
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ),
                REQUEST_ACCESS_LOCATION_PERMISSION
            )
        }
    }

    private fun requestPermissionsRecording() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.RECORD_AUDIO),
            REQUEST_RECORD_AUDIO_PERMISSION
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        mesh?.stop()
    }

}
